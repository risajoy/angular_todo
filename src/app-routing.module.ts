import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TodoTasksComponent } from './app/todos/todo-tasks/todo-tasks.component';
import { TodoAddComponent } from './app/todos/todo-add/todo-add.component';
import { UserListComponent } from './app/users/user-list/user-list.component';
import { UserAddComponent } from './app/users/user-add/user-add.component';
import { UserTodosComponent } from './app/users/user-todos/user-todos.component';

const routes: Routes = [
  { path: '', redirectTo: '/todos', pathMatch: 'full' },
  { path: 'todos', component: TodoTasksComponent },
  { path: 'addTodo', component: TodoAddComponent },
  { path: 'todo/detail/:id', component: TodoAddComponent },
  { path: 'users', component: UserListComponent },
  { path: 'addUser', component: UserAddComponent },
  { path: 'user/detail/:id', component: UserAddComponent },
  { path: 'user/todos/:id', component: TodoTasksComponent },
  // { path: 'user/todos/:id', component: UserTodosComponent }


];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
