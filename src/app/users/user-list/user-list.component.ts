import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  user: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
      this.reloadData();
  }
  reloadData() {
    this.userService.findAll()
    .subscribe(data => {this.user = data;
    });

  }
  // tslint:disable-next-line:variable-name
  deleteUser(user_id: number) {
    this.userService.deleteUser(user_id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
        error => console.log(error));

  }

}
