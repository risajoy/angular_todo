import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Todo } from 'src/app/model/todo';
import { TodoServiceService } from 'src/app/todo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-user-todos',
  templateUrl: './user-todos.component.html',
  styleUrls: ['./user-todos.component.css']
})
export class UserTodosComponent implements OnInit {
  user: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private todoService: TodoServiceService) { }

  ngOnInit() {
    // this.reloadData();
  }

  // reloadData(): Observable<Todo> {
  //   return this.todoService.findToDoByUserID(this.user.user_id)
  //   .subscribe();

  // }
}
