import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TodoTasksComponent } from './todos/todo-tasks/todo-tasks.component';
import { TodoServiceService } from './todo.service';
import { TodoAddComponent } from './todos/todo-add/todo-add.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { UserService } from './user.service';
import { UserAddComponent } from './users/user-add/user-add.component';
import { UserTodosComponent } from './users/user-todos/user-todos.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoTasksComponent,
    TodoAddComponent,
    UserListComponent,
    UserAddComponent,
    UserTodosComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  providers: [TodoServiceService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
