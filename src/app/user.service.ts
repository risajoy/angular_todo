import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Todo } from './model/todo';
import { User } from './model/user';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {
  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8081/users';
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }
  save(user: User) {
    return this.http.post<User>(this.usersUrl, user);
  }
  // tslint:disable-next-line:variable-name
  getUser(user_id: number): Observable<any> {
    return this.http.get(`${this.usersUrl}/${user_id}`);
  }
  // tslint:disable-next-line:variable-name
  deleteUser(user_id: number): Observable<any> {
    return this.http.delete(`${this.usersUrl}/${user_id}`);
  }

}
