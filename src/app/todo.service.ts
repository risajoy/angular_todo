import { Todo } from './model/todo';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TodoServiceService {
  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8081/todos';
  }

  findAll(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.usersUrl);
  }
  save(todo: Todo) {
    return this.http.post<Todo>(this.usersUrl, todo);
  }
  deleteTodo(id: number): Observable<any> {
    return this.http.delete(`${this.usersUrl}/${id}`, { responseType: 'text' });
  }
  getTodo(id: number): Observable<any> {
    return this.http.get(`${this.usersUrl}/${id}`);

  }
  // findToDoByUserID() {

  // }
}
