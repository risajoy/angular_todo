import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/model/todo';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoServiceService } from 'src/app/todo.service';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css'],

})
export class TodoAddComponent implements OnInit {

  todo: Todo;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private todoService: TodoServiceService) {
      this.todo = new Todo();
    }

  ngOnInit() {
    if (this.todo) {
    this.todo = new Todo();
    this.todo.id = this.route.snapshot.params.id;

    this.todoService.getTodo(this.todo.id)
      .subscribe(data => {
        console.log(data);
        this.todo = data;
      }, error => console.log(error));
  }

  }
  onSubmit() {
    console.log(this.todo.status);
    this.todoService.save(this.todo).subscribe(result => this.gotoTodoList());
  }
  gotoTodoList() {
    this.router.navigate(['/todos']);
  }

}
