import { Todo } from 'src/app/model/todo';
import { TodoServiceService } from 'src/app/todo.service';
import { OnInit, Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-todo-tasks',
  templateUrl: './todo-tasks.component.html',
  styleUrls: ['./todo-tasks.component.css']
})
export class TodoTasksComponent implements OnInit {

  todo: Todo[];

  constructor(private todoService: TodoServiceService) { }

  ngOnInit() {
    this.reloadData();
    }
    reloadData() {
      this.todoService.findAll()
      .subscribe(data => {this.todo = data;
    });
  }
    deleteTodo(id: number) {
      this.todoService.deleteTodo(id)
        .subscribe(
          data => {
            console.log(data);
            this.reloadData();
          },
          error => console.log(error));
    }
}
